<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MainController extends CI_Controller {

	function __construct() {
	    parent::__construct();
	    $this->load->helper(array('array_helper'));    
	    $this->load->model('mainModel');
	}
	public function index(){

		// yahan humne content k name se variable banaya h aur uskko pass krwaya h data se(data=array)
       $data['content'] = 'index';
       $this->load->view('Template/template',$data);
	}
	public function Add(){
       $data['roles']=$this->mainModel->getRole('true');
       $data['content'] = 'user';
       $this->load->view('Template/template',$data);
	}
	public function getList(){
     
     //use model 
       $data['result']=$this->mainModel->getUsers();
       $data['content'] = 'table';
       $this->load->view('Template/template',$data);
	}
	public function command(){
       $post=$this->input->post();
       $this->load->library('form_validation');
       $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

      $this->form_validation->set_rules('name', 'Username', 'required|max_length[10]');
      $this->form_validation->set_rules('email', 'Email', 'required|max_length[25]|min_length[12]');
      $this->form_validation->set_rules('password', 'password','required|max_length[16]|min_length[8]');
      if ($this->form_validation->run() == FALSE)
      {
              // dd(validation_errors());
        $data['roles']=$this->mainModel->getRole('true');
          $data['content'] = 'user';
          return  $this->load->view('Template/template',$data);                    
      }












       $active =0;
       if(isset($post['active'])){ 
       	$active = 1;
       }

      $Data=array(
       	'name'		=>$post['name'],
       	'email'	=>$post['email'],
       	'password'	=>$post['password'],
        'roleId'   =>$post['roleId'],
       	'active'	=> $active,
       );

       $this->db->insert('users',$Data);
       redirect(base_url().'MainController/getList');
       }






       public function role(){
        $data['content'] = 'add2';
        $this->load->view('Template/template',$data);      
       }
       public function roleList(){
       $data['result']=$this->mainModel->getRole();       
       $data['content'] = 'table2';
       $this->load->view('Template/template',$data);
       }  

    public function inserted(){
       $post=$this->input->post();
       if(!empty($post)){ 
           $Data=array(
            'role'    =>$post['role'],
            'description' =>$post['description'],
           );
           $this->db->insert('role',$Data);
           redirect(base_url().'MainController/roleList');
       }else{
        redirect(base_url().'MainController/roleList');
       }
    }

}






