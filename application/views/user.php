
 <div class="container"> 
        <div class="text-right"> 
            <a href="<?php echo base_url() ?>MainController/getlist"><button class="btn btn-danger mx-3 justify-content-center">show table</button></a> 
             
        </div> 
      </div>
  <div class="container col-10 mt-4">


            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Examples</h3>
                

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form"  action="<?php echo base_url()?>MainController/command" method="POST" >
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Name" name="name" >
                    <div class="error"><?php echo form_error('name'); ?></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">email address here</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"
                    name="email" required>
                      <div class="error"><?php echo form_error('email'); ?></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Role</label>
                    <select class="select col-12 form-control" name="roleId" required>
                      <option>Select Role</option>
                      <?php foreach ($roles as $k => $value) { ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['role']; ?></option>
                      <?php } ?>

                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password here</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
                    <div class="error"><?php echo form_error('password'); ?></div>
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputFile">File input here</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div> -->
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="active" value="1">
                    <label class="form-check-label" for="exampleCheck1">Active</label>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
 
</div>