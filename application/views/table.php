
<div class="container"> 
    <div class="text-right mb-3"> 
            <a href="<?php echo base_url() ?>MainController/Add"><button class="btn btn-danger mx-3 ">Add user</button></a> 
     </div> 
</div>
<div class="container">
  <div class="row">
    <div class="col-12">
     <div class="table-responsive">
        <table class="table table-bordered table-striped dataTable ">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col"> Name</th>
              <th scope="col">Email</th>
              <th scope="col">Role</th>
              <th scope="col">Status</th>
              <th scope="col">Actions</th>

            </tr>
          </thead>
          <tbody>
         <?php 
         foreach ($result as $k => $value) { ?>
                   
            <tr>
              <th scope="row"><?php echo $k+1; ?></th>
              <td> <?php echo $value['name'] ?></td>
              <td> <?php echo $value['email'] ?></td>
              <td> <?php echo $value['role'] ?></td>
              <td><?php echo $value['active'] ?></td>
              <td>
                <button type="button" class="btn btn-primary"><i class="far fa-eye"></i></button>
                <button type="button" class="btn btn-success"><i class="fas fa-edit"></i></button>
              <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
              </td>
            </tr>
   <?php  } ?>
          </tbody>
      </table>
      </div>
    </div>
  </div>
</div>