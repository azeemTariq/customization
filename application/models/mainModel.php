<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class mainModel extends CI_Model
{
    public function getUsers(){
      return   $this->db->query("
        SELECT
          users.id,
          users.name,
          users.email,
          role.role,
          users.active
        FROM
          users
        JOIN
          role ON users.roleId = role.id
          ORDER BY users.id DESC" )->result_array();

    }    
    public function getRole($type=''){
     $orderby='';
     if($type==''){
         $orderby=' order by id DESC ';
     }

      return   $this->db->query(" SELECT * FROM role $orderby ")->result_array();
    }

}


?>