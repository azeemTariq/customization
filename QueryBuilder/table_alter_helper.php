<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

if (! function_exists('global_variables')) {

    /*
     * Execute All query
     * $surface[maxId]['UpdateStat']="your query";
     *
     * Check Primary key before Add primary key on table
     * $surface[maxId]['CheckPrimary']="tablename";
     *
     * Check Foreign key before Add foreign key on table
     * $surface[maxId]['checkFK']="Tablename,RefernceTableName,ConstraintName";
     *
     * Check table before add or drop
     * $surface[maxId]['CheckStat']="tablename";
     *
     * Check Column before Alter
     * $surface[maxId]['CheckStat']="tablename,columnname";
     *
     */
    function RunRecord()
    {
        $surface = array();
        $surface[LastKey($surface)+1]['CheckStat']   =   "azeem,trans_date";
        $surface[LastKey($surface)]['UpdateStat']    = "ALTER TABLE `azeem` ADD `trans_date` VARCHAR(233) NOT NULL AFTER `id`";
        $surface[LastKey($surface)+1]['CheckStat']="admin";
        $surface[LastKey($surface)]['UpdateStat']    = "CREATE TABLE `user` (
                      `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
                      `User` char(80) COLLATE utf8_bin NOT NULL DEFAULT '',
                      `Password` char(41) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
                      `Select_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Insert_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Update_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Delete_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Create_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Drop_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Reload_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Shutdown_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Process_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `File_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Grant_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `References_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Index_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Alter_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Show_db_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Super_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Execute_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Repl_slave_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Repl_client_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Create_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Show_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Create_user_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Event_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Trigger_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `Create_tablespace_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `ssl_type` enum('','ANY','X509','SPECIFIED') CHARACTER SET utf8 NOT NULL DEFAULT '',
                      `ssl_cipher` blob NOT NULL,
                      `x509_issuer` blob NOT NULL,
                      `x509_subject` blob NOT NULL,
                      `max_questions` int(11) UNSIGNED NOT NULL DEFAULT '0',
                      `max_updates` int(11) UNSIGNED NOT NULL DEFAULT '0',
                      `max_connections` int(11) UNSIGNED NOT NULL DEFAULT '0',
                      `max_user_connections` int(11) NOT NULL DEFAULT '0',
                      `plugin` char(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
                      `authentication_string` text COLLATE utf8_bin NOT NULL,
                      `password_expired` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `is_role` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
                      `default_role` char(80) COLLATE utf8_bin NOT NULL DEFAULT '',
                      `max_statement_time` decimal(12,6) NOT NULL DEFAULT '0.000000'
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and global privileges'";

            
    return $surface;

    }

    /*
     * Execute All query
     * $surface[LastKey($surface)+1]['UpdateStat']="your query";
     *
     * Check Primary key before Add primary key on table
     * $surface[LastKey($surface)+1]['CheckPrimary']="tablename";
     * $surface[LastKey($surface)]['UpdateStat'] ="ALTER QUERY PASTE HERE";
     *
     * Check Foreign key before Add foreign key on table
     * $surface[LastKey($surface)+1]['checkFK']="Tablename,RefernceTableName,ConstraintName";
     * $surface[LastKey($surface)]['UpdateStat'] ="ALTER QUERY PASTE HERE";
     *
     * Check table before add or drop
     * $surface[LastKey($surface)+1]['CheckStat']="tablename";
     * $surface[LastKey($surface)]['UpdateStat'] ="ALTER QUERY PASTE HERE";
     *
     * Check Column before Alter
     * $surface[LastKey($surface)+1]['CheckStat']="tablename,columnname";
     * $surface[LastKey($surface)]['UpdateStat'] ="ALTER QUERY PASTE HERE";
     *
     * Check View before Alter
     * $surface[LastKey($surface)+1]['CheckView']="view name";   
     * $surface[LastKey($surface)]['UpdateStat'] ="ALTER QUERY PASTE HERE"; 
     *
     * Check menu before Alter
     * $surface[LastKey($surface)+1]['CheckMenu']="controller,function";   
     * $surface[LastKey($surface)]['UpdateStat'] ="ALTER QUERY PASTE HERE";  
     */
    function ReturnFirst($number)
    {
        $recorde = RunRecord();
        $return_number = array();
        foreach ($recorde as $key => $value) {
            if (($key >= $number)) {
                $return_number[$key] = $value;
            }
        }
        return $return_number;
    }
}
function LastKey($array){
    $keys= array_keys($array);
    return end($keys);
}

?>