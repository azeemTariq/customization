<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Login extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }
    public function index()
    {
        $this->load->view('login');
    }
    public function executeQuery(){
        $this->load->helper('table_alter_helper');
    }
    public function check_database(){
                    $this->load->helper('table_alter_helper');
                    $this->db->select('par_data');
                    $this->db->from('parameter');
                    $this->db->where(array('par_code' => 'UPDATE'));
                    $GetLastCount=$this->db->get()->result();
                    if($GetLastCount){
                        $GetLastCount = $GetLastCount[0]->par_data + 1;
                    }else{
                        $GetLastCount = 1;
                    }
                    $listQuery = ReturnFirst($GetLastCount);
                     echo sizeof($listQuery);
                }

    public function DbStrutreChanges(){

            $this->load->helper('table_alter_helper');
            $this->db->select('par_data');
            $this->db->from('parameter');
            $this->db->where(array('par_code' => 'UPDATE'));
            $GetLastCount=$this->db->get()->result();

            if($GetLastCount){
                $GetLastCount = $GetLastCount[0]->par_data + 1;
            }else{
                $GetLastCount = 1;
            }

            $check = 0;
            $execute=0;
            $error=array('code'=>0);
            $TotError=array();
            $db_debug = $this->db->db_debug;
            $this->db->db_debug = FALSE;
            $listQuery = ReturnFirst($GetLastCount);
            foreach($listQuery as $key => $list){

                if($error['code']==0 || $error['code']==1062 ){

                        if(isset($list['CheckStat'])){

                         $columns=explode(",", $list['CheckStat']);

                         $exists = $this->checkTblCol($columns[0],$columns[1] ?? null);
                         if($exists==0){
                            $check = $this->db->query("call QueryExecutor(".$this->db->escape($list['UpdateStat']).")");
                        }else{$check=1;}
                    }
                    else if(isset($list['CheckAlter'])){
                     $columns=explode(",", $list['CheckAlter']);
                     $exists = $this->checkTblCol($columns[0],$columns[1] ?? null);
                     if($exists==1){
                        $check = $this->db->query("call QueryExecutor(".$this->db->escape($list['UpdateStat']).")");
                        }else{$check=1;}
                    }
                    elseif(isset($list['CheckPrimary'])){
                        $exists = $this->checkTblCol($list['CheckPrimary'],"PRI");
                        if($exists==0){
                            $check = $this->db->query("call QueryExecutor(".$this->db->escape($list['UpdateStat']).")");
                        }else{$check=1;}
                    }
                     elseif(isset($list['checkFK'])){
                                    $columns=explode(",", $list['checkFK']);
                                    $exists = $this->checkFK($columns[0],$columns[1],$columns[2]);
                                    if($exists==0){
                                        $check = $this->db->query("call QueryExecutor(".$this->db->escape($list['UpdateStat']).")");
                                    }else{$check=1;}
                                }
                                elseif(isset($list['DropColumn'])){
                                    $columns=explode(",", $list['DropColumn']);
                                    $exists = $this->checkTblCol($columns[0],$columns[1]);
                                    if($exists==1){
                                        $check = $this->db->query("call QueryExecutor(".$this->db->escape($list['UpdateStat']).")");
                                    }else{$check=1;}
                                }
                                elseif(isset($list['CheckView'])){
                                    $exists = $this->CheckView($list['CheckView']);
                                    if($exists==0){
                                        $check = $this->db->query($list['UpdateStat']);
                                    }else{$check=1;}

                                }
                                elseif(isset($list['CheckTrigger'])){
                                    $check = $this->db->query($list['UpdateStat']);
                                }
                                elseif(isset($list['CheckMenu'])){
                                    $columns=explode(",", $list['CheckMenu']);
                                    $exists = $this->checkTblMenu($columns[0],$columns[1] ?? null);
                                    //$check = ($exists==0) ? $this->db->query($list['UpdateStat']) : 1;
                                    if($exists==0){
                                        $check = $this->db->query($list['UpdateStat']);
                                        $this->addPermissions($this->db->insert_id());
                                    }else{
                                        $check = 1 ;
                                    }
                                }
                                else{
                                    $check = $this->db->query("call QueryExecutor(".$this->db->escape($list['UpdateStat']).")");
                                }
                                $execute+=$check->result_id ?? $check;
                                // print_r($execute);
                                
                                $error=$this->db->error();
                                 $this->db->db_debug =$db_debug;
                                if($error['code']==0 || $error['code']==1062){
                                    $end = $key;
                                }
                                $errorArr=$this->db->error();
                                $errorArr['key']=$key;
                                array_push($TotError, $errorArr);
                }     
                                
            }   
                  $this->db->db_debug = $db_debug;
                        if($error['code']!=0 && $error['code']!=1062){
                            $response="Alert! ";
                            foreach ($TotError as $key => $value) {
                                if($value['code']!=0){
                                    $response.='<br> Error on Query '.$value['key'].'. '.$value['message'];
                                }
                            }

                            echo 'alert-danger,'.$response;
                        }

                        if (count($listQuery) > 0 && $execute !=0){
                             print_r($end);
                          $this->Login_model->update_parameter('parameter',array('par_data'=>$end),array('par_code'=>'UPDATE'));
                          
                          
                          $Getversion = $this->Login_model->db_select('par_data','parameter',array('par_code' => 'Rel'));
                          if(isset($Getversion[0])){
                          $relstring =  preg_split('/( |_)/', $Getversion[0]->par_data,-1, PREG_SPLIT_NO_EMPTY);
                        $tag =  $relstring[1]+.01;
                        $newre = 'Ver. '. $tag .'_'.date('dmy');
//                         pre($newre);
                           $this->Login_model->update_parameter(array('par_code'=>'Rel'),'parameter',array('par_data'=>$newre));
                          }
                      }
                  }
                  function checkTblMenu($controller,$function=null){
                    $where="controller = '".$controller."' AND (function = '".$function."' OR function IS NULL)";
                    $check = $this->Login_model->db_select('count(*) as count','admin_module_list',$where);

                    return $check[0]->count;
                }
                  function checkTblCol($table,$column=null){
                    $where=array('TABLE_SCHEMA' => $this->db->database,'TABLE_NAME'=>$table);
                    ($column == "PRI") ? $where['COLUMN_KEY']   =  "PRI" : (($column) ? $where['COLUMN_NAME']   =  $column : "");
                    $check = $this->Login_model->db_select('count(*) as count','information_schema.COLUMNS',$where);
                  ;
                    return $check[0]->count;
                }
                function checkFK($table,$reference,$constrain){
                    $check = $this->Login_model->db_select('count(*) as count','information_schema.REFERENTIAL_CONSTRAINTS',
                        array('CONSTRAINT_SCHEMA'       => $this->db->database,
                          'TABLE_NAME'              =>  $table,
                          'REFERENCED_TABLE_NAME'   =>  $reference,
                          'CONSTRAINT_NAME'         =>  $constrain));
                    return $check[0]->count;
                }
                function CheckView($view){
                    $check = $this->Login_model->db_select('count(*) as count','information_schema.VIEWS',
                        array('table_schema'       => $this->db->database,
                          'TABLE_NAME'              =>  $view));
                    return $check[0]->count;
                }      
               
}

?>