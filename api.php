<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

//require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Api extends CI_Controller
{
        const BY_DEFAULT = 1;   
        const BY_FB = 2;
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('Main_manager');
        //$this->isLoggedIn();
    }

    public function homeScreen()
    {
        $this->load->model('api_model');
        $api_secret = $this->input->post('api_secret');
        if(empty($api_secret))
        {
            //print_r('empty');
            $response['result']['status']   = 'error';
            $response['result']['response'] = "API Secret is missing.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }

        $user_id = $this->Main_manager->validate_api($api_secret);
        if($user_id>0)
        {
            $user_details = $this->Main_manager->selectByOtherCol('id',$user_id,'customers');
            $user_city_id = $user_details[0]['city_id'];

            $getTopDeals = $this->api_model->getTopDeals_homeScreen($user_city_id);
            $getTopRestaurants = $this->api_model->getTopRestaurants_homeScreen($user_city_id);
            $getNewOnApp = $this->api_model->getNewOnApp_homeScreen();
            $getMostSaleOrder = $this->api_model->getMostSaleOrder_homeScreen();

            $getMostPopularRest = array();
            $i = 0;
            foreach ($getMostSaleOrder as $key => $value) {
                    $restaurant_ids = $value->restaurant_id;
                    $branch_ids = $value->branch_id;

            $result = $this->api_model->getMostPopularRest_homeScreen($restaurant_ids,$branch_ids);
            //array_push($getMostPopularRest, $result);
            foreach ($result as $key => $value) {
                $getMostPopularRest[$i] = $value;
                $i++;
            }

            
            }
            //print_r($getMostSaleOrder);
            //die();
            
            $response['result']['status']   = 'success';
            $response['result']['top_food_deals'] = $getTopDeals;
            $response['result']['top_featured_restaurants'] = $getTopRestaurants;
            $response['result']['new_on_foodApp'] = $getNewOnApp;
            $response['result']['most_popular'] = $getMostPopularRest;
            //return array($response, 400);
            echo json_encode($response);
            die();   
        }

    }

    public function restaurant_services()
    {

        $this->load->model('api_model');
        $api_secret = $this->input->post('api_secret');
        $restaurant_type = $this->input->post('type');
        if(empty($api_secret))
        {
            $response['result']['status']   = 'error';
            $response['result']['response'] = "API Secret is missing.";
            echo json_encode($response, 400);
            die();
        }

        $user_id = $this->Main_manager->validate_api($api_secret);
        if($user_id>0)
        {
            $getTypeRestaurants = $this->api_model->getTypeRestaurants($restaurant_type);
        }
            $response['result']['status']   = 'success';
            
            switch ($restaurant_type) {
                case 1:
                    $response['result']['delivery_restaurants'] = $getTypeRestaurants;
                    break;
                case 2:
                    $response['result']['takeaway_restaurants'] = $getTypeRestaurants;
                    break;
                case 3:
                    $response['result']['reservation_restaurants'] = $getTypeRestaurants;
                    break;
                case 3:
                    $response['result']['quick_service_restaurants'] = $getTypeRestaurants;
                    break;
            }
            echo json_encode($response);
            die();
        
    }

        public function most_popular()
    {

        $this->load->model('api_model');
        $api_secret = $this->input->post('api_secret');
        $restaurant_id = $this->input->post('restaurant_id');
        $branch_id = $this->input->post('branch_id');
        if(empty($api_secret))
        {
            //print_r('empty');
            $response['result']['status']   = 'error';
            $response['result']['response'] = "API Secret is missing.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
        $user_id = $this->Main_manager->validate_api($api_secret);
        if($user_id>0)
        {
            $finalData = array();
            $i=0;
            $getMostPopular = $this->api_model->getMostPopular($restaurant_id,$branch_id);
            if(is_array($getMostPopular) || is_object($getMostPopular))
            {

            foreach ($getMostPopular as $key => $value) {
                $item_id = $value->order_item;
                $items = $this->api_model->selectByOtherCol('id',$item_id,'tbl_item');
                foreach ($items as $key => $value) {
                    $data = $value;
                    array_push($finalData, $data);
                }
                //array_push($finalData, $data);

            }
                
            }
            $response['result']['status'] = 'success';
            $response['result']['response'] = $finalData;
            echo json_encode($response);

        }
        else
        {
            $response['result']['status']   = 'error';
            $response['result']['response'] = "Invalid Api Key.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
    }

    public function restaurantList()
    {
        $this->load->model('api_model');
        $api_secret = $this->input->post('api_secret');
        if(empty($api_secret))
        {
            //print_r('empty');
            $response['result']['status']   = 'error';
            $response['result']['response'] = "API Secret is missing.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
        $user_id = $this->Main_manager->validate_api($api_secret);
        if($user_id>0)
        {
            $list = $this->api_model->restaurantList_api();
            $data = array();
        $i=0;
        foreach ($list as $key => $value) {
            $restaurants_primary_id = $value->id;
            $getFoodTypeForRestList = $this->api_model->getFoodTypeForRestList($restaurants_primary_id);
            $getOrderTypeForRestList = $this->api_model->getOrderTypeForRestList($restaurants_primary_id);
            //$id = $value->user_id;
            $data[$i] = $value;
            $data[$i]->food_types = $getFoodTypeForRestList;
            $data[$i]->order_types = $getOrderTypeForRestList;
            //$data[$i]->profile_path = $userPicAndCover[0]['profile_path'];
            //$data[$i]->cover_path = $userPicAndCover[0]['cover_path'];
            //print_r($data);
            $i++;
        }
            
            $response['result']['status'] = 'success';
            $response['result']['response'] = $data;
            echo json_encode($response, true);
            //$response['result']['status'] = 'success';
            //$response['result']['response'] = $list;
            //echo json_encode($response);
            //print_r($list);
        }
        else
        {
            $response['result']['status']   = 'error';
            $response['result']['response'] = "Invalid Api Key.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
        //print_r($user_id);
    }

    public function restaurantMenu_list()
    {
        $this->load->model('api_model');
        $api_secret = $this->input->post('api_secret');
        $restaurant_id = $this->input->post('restaurant_id');
        $branch_id = $this->input->post('branch_id');
        if(empty($api_secret))
        {
            //print_r('empty');
            $response['result']['status']   = 'error';
            $response['result']['response'] = "API Secret is missing.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
        $user_id = $this->Main_manager->validate_api($api_secret);
        if($user_id>0)
        {
            $getResult = $this->api_model->getRestaurantMenu_list($restaurant_id,$branch_id);

            $data = array();
        $i=0;
        foreach ($getResult as $key => $value) {

            $restaurants_primary_id = $value->id;

            $data[$i] = $value;

            $i++;
        }
        $cat_name = $this->api_model->selectByTwoCol('restaurant_id',$restaurant_id,'branch_id',$branch_id,'tbl_item_category');
        $data2 = array();
        $i=0;
            foreach ($cat_name as $key => $value) {
                # code...
                $category_id = $value->id;
                $getItems = $this->Main_manager->selectByOtherCol('category_id',$category_id,'tbl_item');
                $category_name = $value->category;
                $data2[$i] = new stdClass;
                $data2[$i]->$category_name = $getItems;

                //echo substr($data2[$i]->$category_name, 1,-1);
                $i++;
                //print_r($value['category']);
            }

            $response['result']['status'] = 'success';
            $response['result']['restaurant_details'] = $data[0];
            //$data2 = trim($data2, '{}');
            //echo substr($data2, 1, -1);
            // var_dump($data2[0]);
            //$test = array('1','2');
            $response['result']['categories'] = $data2;
            echo json_encode($response);
            //$getResult = $this->Main_manager->selectByTwoCol('restaurant_id',$restaurant_id,'branch_id',$branch_id,'restaurants');
            //print_r($getResult);
        }
        else
        {
            $response['result']['status']   = 'error';
            $response['result']['response'] = "Invalid Api Key.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
    }

    public function restaurantMenu_list2()
    {
        $this->load->model('api_model');
        $api_secret = $this->input->post('api_secret');
        $restaurant_id = $this->input->post('restaurant_id');
        $branch_id = $this->input->post('branch_id');
        if(empty($api_secret))
        {
            //print_r('empty');
            $response['result']['status']   = 'error';
            $response['result']['response'] = "API Secret is missing.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
        $user_id = $this->Main_manager->validate_api($api_secret);
        if($user_id>0)
        {
            $getResult = $this->api_model->getRestaurantMenu_list($restaurant_id,$branch_id);

            $data = array();
        $i=0;
        foreach ($getResult as $key => $value) {

            $restaurants_primary_id = $value->id;

            $data[$i] = $value;

            $i++;
        }
        $cat_name = $this->api_model->selectByTwoCol('restaurant_id',$restaurant_id,'branch_id',$branch_id,'tbl_item_category');
        $data2 = array();
        $i=0;
            foreach ($cat_name as $key => $value) {
                # code...
                $category_id = $value->id;
                $getItems = $this->Main_manager->selectByOtherCol('category_id',$category_id,'tbl_item');
                $category_name = $value->category;
                $data2[$i] = new stdClass;
                $data2[$i]->$category_name = $getItems;

                //echo substr($data2[$i]->$category_name, 1,-1);
                $i++;
                //print_r($value['category']);
            }

//             $myArray = array();

// //set up the nested associative arrays using literal array notation
// $firstArray = array("id" => 1, "data" => 45);
// $secondArray = array("id" => 3, "data" => 54);

// //push items onto main array with bracket notation (this will result in numbered indexes)
// $myArray[] = $firstArray;
// $myArray[] = $secondArray;

//convert to json
//$json = json_encode($myArray);

            $response['result']['status'] = 'success';
            $response['result']['restaurant_details'] = $data[0];
            //$data2 = trim($data2, '{}');
            //echo substr($data2, 1, -1);
            // var_dump($data2[0]);
            //$test = array('1','2');
            $response['result']['categories'] = $data2;
            echo json_encode($response);
            //echo str_replace(array('[', ']'), '', htmlspecialchars(json_encode($response), ENT_NOQUOTES));
            //$getResult = $this->Main_manager->selectByTwoCol('restaurant_id',$restaurant_id,'branch_id',$branch_id,'restaurants');
            //print_r($getResult);
        }
        else
        {
            $response['result']['status']   = 'error';
            $response['result']['response'] = "Invalid Api Key.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
    }



    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'FoodApp : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }

    public function table_reservation()
    {
        $this->load->model('api_model');
        $api_secret = $this->input->post('api_secret');
        $restaurant_id = $this->input->post('restaurant_id');
        $branch_id = $this->input->post('branch_id');
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $no_persons = $this->input->post('no_persons');
        $table_no = $this->input->post('table_no');

        if(empty($api_secret))
        {
            //print_r('empty');
            $response['result']['status']   = 'error';
            $response['result']['response'] = "API Secret is missing.";
            //return array($response, 400);
            echo json_encode($response, 400);
            die();
        }
        else
        {
            // $query = $this->db->query('SELECT table_no FROM dinein_table
            //                 WHERE is_reserved = 0
            //                 LIMIT 1');
            // if($query->num_rows()>0)
            // {
            //     $available_table = $query->result_array();
            //     $available_table = $available_table[0]['table_no'];
            // }
            // else
            // {
            //     $response['result']['status']   = 'error';
            //     $response['result']['response'] = "Table not available.";
            //     //return array($response, 400);
            //     echo json_encode($response);
            //     die();
            // }

        $user_id = $this->Main_manager->validate_api($api_secret);
        $data = array(
            'userId' => $user_id,
            'orderTypeId' => 3,
            'restaurant_id' => $restaurant_id,
            'branch_id' => $branch_id,
            'orderStatus' => 0
            );
        $order_id = $this->Main_manager->insert($data,'tbl_order');
        if($order_id>0)
        {

            $data = array(
                'order_id' => $order_id,
                'table_no' => $table_no,
                'user_id'  => $user_id,
                'reserved_time' => $time,
                'reserved_date' => $date,
                'no_of_persons' => $no_persons
            );

            $reserved_tables = $this->Main_manager->insert($data,'reserved_tables');
            if($reserved_tables>0)
            {
                
            //$data = array('is_reserved'=>1);
            //$updateDineIn = $this->Main_manager->updateByOtherCol('table_no',$available_table, $data, 'dinein_table');

                $response['result']['status']   = 'success';
                $response['result']['response'] = "Your Request has been sent for table no ".$table_no;
                //return array($response, 400);
                echo json_encode($response);
                die();
            }

        }
            
        }
        }
    
    
    public function placeOrder()
    {
        $user_id = $this->input->post('user_id');
        $order_type = $this->input->post('order_type');
        $sub_total = $this->input->post('sub_total');
        $comment = $this->input->post('comment');
        $restaurant_id = $this->input->post('restaurant_id');
        $branch_id = $this->input->post('branch_id');
        $item_id = $this->input->post('item_id');
        $qty = $this->input->post('item_qty');
        $table_id = $this->input->post('table_id');

        // print_r($item_id);
        // print_r($qty);
        // die();
        $data = array(
            'userId' => $user_id,
            'orderTypeId' => $order_type,
            'sub_total' => $sub_total,
            'comment' => $comment,
            'restaurant_id' => $restaurant_id,
            'branch_id' => $branch_id,
            'orderStatus' => 0
            );
        $order_id = $this->Main_manager->insert($data,'tbl_order');
        $finalData = array();
        if($order_id>0)
        {
            if($order_type == 4)
            {
                $query = $this->db->query('UPDATE dinein_table SET is_reserved_qs = 1 WHERE table_id = "'.$table_id.'" AND restaurant_id = "'.$restaurant_id.'" AND branch_id = "'.$branch_id.'"');
                $data = array(
                    'order_id' => $order_id,
                    'table_no' => $table_id,
                    'user_id'  => $user_id,
                    'on_car'   => 0,
                    'on_table' => 1,
                    'car_no'   => 0,
                    'restaurant_id' => $restaurant_id,
                    'branch_id' => $branch_id
                );

                $this->Main_manager->insert($data,'reserved_tables_qs');
            }
            $i=0;
            foreach ($item_id as $key => $value) {
                $data = array(
                    'order_id' => $order_id,
                    'order_item'  => $value,
                    'order_qty'      => $qty[$i]
                );
                $i++;

                $complete_order = $this->Main_manager->insert($data,'tbl_order_items');
            }
            if($order_id>0){
            $finalData['status'] = 1;
            }
            else
            {
            $finalData['status'] = 0;
            }   
            }

        
            echo json_encode($finalData);
    }

    public function view_notif()
    {
        $response = '';
        $restaurant_id = $this->session->userdata('restaurant_id_panel');
        $branch_id = $this->session->userdata('branch_id_panel');
        $query = $this->db->query("UPDATE tbl_order SET notify=1 WHERE restaurant_id=".$restaurant_id." AND branch_id = ".$branch_id."");
        if($query>0)
        {
            $s_query = $this->db->query("SELECT tbl_order.id,tbl_order.comment,tbl_order.create_on,order_type.type from tbl_order inner join order_type on order_type.id = tbl_order.orderTypeId where restaurant_id = ".$restaurant_id." AND branch_id = ".$branch_id." ORDER BY id DESC");
            if($s_query->num_rows()>0)
            {
                $res = $s_query->result_array();
                //print_r($res);
                foreach ($res as $key => $value) {
                    //print_r("user_id ".$value['userId']);
    //                 $response = $response . "<div class='notification-item'>" .
    // "<div class='notification-user_id'><a href='newOrdersList'>". $value['userId'] . "</a></div>" . 
    // "<div class='notification-comment'><a href='#'>" .$value['comment']  . "</a></div>" .
    // "</div>";

    $response = $response . "
  <a href=#><li class=notification>
      <div class=media>
        <div class=media-left>
          <div class=media-object>
            Order# ".$value['id']."
          </div>
        </div>
        <div class=media-body>
          <strong class=notification-title>Type: <a href=#>".$value['type']."</a><br> Comment: <a href=#>".$value['comment']."</a></strong>

          <div class=notification-meta>
            <small class=timestamp>".$value['create_on']."</small>
          </div>
        </div>
      </div>
  </li><hr></a>";
                }
            }            
        }
         if(!empty($response)) {
    print $response;
}

// $sql="select * from comments ORDER BY id DESC limit 5";
// $result=mysqli_query($conn, $sql);

// $response='';
// while($row=mysqli_fetch_array($result)) {
//     $response = $response . "<div class='notification-item'>" .
//     "<div class='notification-subject'>". $row["subject"] . "</div>" . 
//     "<div class='notification-comment'>" . $row["comment"]  . "</div>" .
//     "</div>";
// }
// if(!empty($response)) {
//     print $response;
// }
    }

    public function register()
    {
        $user_name = @trim(addSlashes($this->input->post('user_name')));
        $date_ob = @trim(addslashes($this->input->post('date_ob')));
        $password = $this->input->post('password');
        $email = @trim(addslashes($this->input->post('email')));
        $phone = @trim(addslashes($this->input->post('phone')));
        $address = @trim(addslashes($this->input->post('address')));
        $office_address = @trim(addslashes($this->input->post('office_address')));
        $fb_link = @trim(addslashes($this->input->post('fb_link')));

        if(!empty($email))
        {
            $checkEmail = $this->Main_manager->selectByOtherCol('email',$email,'customers');
            if($checkEmail>0)
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Email Already Exists.";
                echo json_encode($response);
                die();

            }
            else
            {
                $email = $email;
            }

        }
        if(!empty($user_name))
        {
            $checkUserName = $this->Main_manager->selectByOtherCol('username',$user_name,'customers');
            if($checkUserName>0)
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "User name Already Exists.";
                echo json_encode($response);
                die();
            }
            else
            {
                $user_name = $user_name;
            }

        }

        if(!empty($password))
        {
            if(strlen($password) < 6)
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Minimum password lenght should be 6.";
                echo json_encode($response);
                die();
            }
            else
            {

            $password = md5($password);
            }
        }

        $data = array(
            'username' => $user_name,
            'email' => $email,
            'password' => $password,
            'address' => $address,
            'phone' => $phone,
            'office_address' => $office_address,
            'date_ob' => $date_ob,
            'fb_id' => $fb_link
            );

        $insert_id = $this->Main_manager->insert($data,'customers');
        if($insert_id>0)
        {
            $api_id = $this->Main_manager->generateApiID(array($insert_id));
            $api_secret = $this->Main_manager->generateApiSecret($api_id);
            $data = array(
                    'customer_id' => $insert_id,
                    'api_id' => $api_id,
                    'api_secret' => $api_secret
                    );
            $createApiKeys = $this->Main_manager->insert($data,'api_keys');
            if($createApiKeys>0)
            {
            $response['result']['status']   = 'success';
            $response['result']['response'] = "User ID ".$insert_id." has inserted";
            echo json_encode($response);
            die();
                
            }
        }



    }

    public function login()
    {

        $this->load->model('Main_manager');
        //$this->CI->load->model('users_model');
        /*
         * addSlashes Just to Secure From SQL Injection
         */
        $email         = @trim(addslashes($this->input->post('email')));
        $password      = @$this->input->post('password');
        $signin_type   = @$this->input->post('signin_type');
        $fb_id         = @trim(addslashes($this->input->post('fb_id')));

        if(!empty($signin_type))
        {
            if($signin_type != self::BY_FB &&  $signin_type != self::BY_DEFAULT)
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Invalid Signin Type.";
                echo json_encode($response);
            }
        }
        if($signin_type == self::BY_DEFAULT)
        {
            if(empty($email))
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Email is missing.";
                // return array($response, 400);
                echo json_encode($response);
                die();
            }
            if(empty($password))
            {
                $response['result']['status']       = 'error';
                $response['result']['response']     = "Password is missing.";
                echo json_encode($response);
                die();
                //return array($response, 400);
            }
            if(!empty($email) || !empty($password))
            {
                $checkLogin = $this->Main_manager->selectByTwoCol('email',$email,'password',md5($password),'customers');
                if($checkLogin>0)
                {
                $cus_id = $checkLogin[0]['id'];
                $getApiKeys = $this->Main_manager->selectByOtherCol('customer_id',$cus_id,'api_keys');
                $getApiKeys[0]['api_secret'];

                //print_r($checkLogin);
                $response['result']['status']       = 'success';
                $response['result']['response']     = "User login successful.";
                $response['data']['api_secret']     = $getApiKeys[0]['api_secret'];
                //$response['data']['api_id']         = $api_key[0]->api_id;
                $response['data']['user data']     = $checkLogin[0];
                echo json_encode($response);
                die();
                }
                else
                {
                $response['result']['status']       = 'error';
                $response['result']['response']     = "Ivalid Credentials";
                echo json_encode($response);
                die();
                }
            }
            //$data = $this->Main_manager->selectByOtherCol('email',$email,'customers');
            //$signin_type = $signin_type;
            //$data = $this->CI->users_model->get_record_for_field('email', $email);
        }
        // elseif($signin_type == self::BY_FB)
        // {
        //     $fb_id = $this->input->post('fb_id');
        //     $fb_token = $this->input->post('fb_token');
            
        //     if(empty($fb_id))
        //     {
        //         $response['result']['status']       = 'error';
        //         $response['result']['response']     = "Facebook ID is missing.";
        //         //return array($response, 400);
        //         echo json_encode($response);
        //         die();
        //     }
        //     $data = $this->Main_manager->selectByOtherCol('fb_id',$fb_id,'customers');
        //     //$data   = $this->CI->users_model->get_record_for_field('fb_id', $fb_id);
        // }
        // else
        // {
        //     $response['result']['status']   = 'error';
        //     $response['result']['response']     = "Signin Type is Missing.";
        //     //return array($response, 400);
        //     echo json_encode($response);
        //     die();
        // }

        // if(empty($data))
        // {
        //     $response['result']['status']   = 'error';
        //     $response['result']['response']     = "Invalid Crediantials.";
        //     //return array($response, 400);
        //     echo json_encode($response);
        //     die();
        // }
        // else
        // {
        //     if($data[0]->password == md5($password) || $signin_by != Users_model::BY_CUSTOM)
        //     {
        //         $this->CI->load->model('api_keys_model');

        //         $api_key    = $this->CI->api_keys_model->get_record_for_field('user_id', $data[0]->user_id);


        //         $this->CI->users_model->user_id     = $data[0]->user_id;
        //         $this->CI->users_model->user_agent  = $_SERVER['HTTP_USER_AGENT'];
        //         $this->CI->users_model->save();
                
        //         $where  = array('users.user_id' => $data[0]->user_id);
        //         $user_data = $this->CI->users_model->get_record_withPictue('*',$where);
                
        //         $response['result']['status']       = 'success';
        //         $response['result']['response']     = "User login successful.";
        //         $response['data']['api_secret']     = $api_key[0]->api_secret;
        //         $response['data']['api_id']         = $api_key[0]->api_id;
        //         $response['data']['user data']     = $user_data[0];
        //         return array($response, 200);
        //     }
        //     else
        //     {
        //         $response['result']['status']   = 'error';
        //         $response['result']['response'] = "Invalid password.";
        //         return array($response, 400);
        //     }
        // }
    }
    /**
     * This function is used to load the user list
     */

}

?>